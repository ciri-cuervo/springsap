# Spring SAP #

Web services developed with Spring WS for getting SAP messages.

### How do I get set up? ###

+ Download source code
    * `git clone https://your-username@bitbucket.org/ciri-cuervo/springsap.git`
+ Summary of set up
    * You need to have Maven installed
    * Inside project directory: `mvn clean package eclipse:clean eclipse:eclipse`
+ Deployment instructions
    * You can run the application with Jetty server: `mvn jetty:run`
    * It will be accessible at: `http://localhost:8080/`
    * WAR file is generated inside `target` directory after running: `mvn clean package`