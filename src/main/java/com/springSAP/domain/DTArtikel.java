//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: [TEXT REMOVED by maven-replacer-plugin]
//


package com.springSAP.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DT_Artikel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DT_Artikel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ArtikelNr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Omschrijving" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="GoederenGroep" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Eenheid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DT_Artikel", propOrder = {
    "artikelNr",
    "omschrijving",
    "goederenGroep",
    "status",
    "eenheid"
})
public class DTArtikel {

    @XmlElement(name = "ArtikelNr", required = true)
    protected String artikelNr;
    @XmlElement(name = "Omschrijving", required = true)
    protected String omschrijving;
    @XmlElement(name = "GoederenGroep", required = true)
    protected String goederenGroep;
    @XmlElement(name = "Status", required = true)
    protected String status;
    @XmlElement(name = "Eenheid", required = true)
    protected String eenheid;

    /**
     * Gets the value of the artikelNr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArtikelNr() {
        return artikelNr;
    }

    /**
     * Sets the value of the artikelNr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArtikelNr(String value) {
        this.artikelNr = value;
    }

    /**
     * Gets the value of the omschrijving property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOmschrijving() {
        return omschrijving;
    }

    /**
     * Sets the value of the omschrijving property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOmschrijving(String value) {
        this.omschrijving = value;
    }

    /**
     * Gets the value of the goederenGroep property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGoederenGroep() {
        return goederenGroep;
    }

    /**
     * Sets the value of the goederenGroep property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGoederenGroep(String value) {
        this.goederenGroep = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the eenheid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEenheid() {
        return eenheid;
    }

    /**
     * Sets the value of the eenheid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEenheid(String value) {
        this.eenheid = value;
    }

}
