package com.springSAP.config;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.server.EndpointInterceptor;
import org.springframework.ws.server.endpoint.interceptor.PayloadLoggingInterceptor;
import org.springframework.ws.soap.server.endpoint.interceptor.PayloadValidatingInterceptor;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.XsdSchemaCollection;
import org.springframework.xml.xsd.commons.CommonsXsdSchemaCollection;

@Configuration
@EnableWs
@ComponentScan("com.springSAP.endpoint")
public class WebServiceConfig extends WsConfigurerAdapter {

	private static final String NAMESPACE_URI = "http://ugent.be/MM/ExportMat";

	@Bean(name = "ugent-export-art")
	public DefaultWsdl11Definition wsdl11Definition(XsdSchemaCollection springSAPSchemaCollection) {
		DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
		wsdl11Definition.setPortTypeName("ugentExportArtPort");
		wsdl11Definition.setLocationUri("/ws");
		wsdl11Definition.setSchemaCollection(springSAPSchemaCollection);
		wsdl11Definition.setTargetNamespace(NAMESPACE_URI);
		wsdl11Definition.setRequestSuffix("_ExportMat");
		return wsdl11Definition;
	}

	@Bean
	public XsdSchemaCollection springSAPSchemaCollection() {
		Resource[] resources = new Resource[] {
				new ClassPathResource("schema/Schema1.xsd"),
				new ClassPathResource("schema/Schema2.xsd"),
				new ClassPathResource("schema/MainSchema.xsd")
		};
		CommonsXsdSchemaCollection schemaCollection = new CommonsXsdSchemaCollection(resources);
		schemaCollection.setInline(true);
		return schemaCollection;
	}

	@Override
	public void addInterceptors(List<EndpointInterceptor> interceptors) {
		// add logging interceptor
		interceptors.add(new PayloadLoggingInterceptor());

		// add validating interceptor
		PayloadValidatingInterceptor validatingInterceptor = new PayloadValidatingInterceptor();
		validatingInterceptor.setXsdSchemaCollection(springSAPSchemaCollection());
		validatingInterceptor.setValidateResponse(true);
		interceptors.add(validatingInterceptor);
	}

}
