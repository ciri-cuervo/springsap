package com.springSAP.endpoint;

import javax.xml.bind.JAXBElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.springSAP.domain.DTExportMat;

@Endpoint
public class SAPEndpoint {

	private static final Logger LOGGER = LoggerFactory.getLogger(SAPEndpoint.class);

	@PayloadRoot(namespace = "http://ugent.be/MM/ExportMat", localPart = "MT_ExportMat")
	@ResponsePayload
	public void receiveMessage(@RequestPayload JAXBElement<DTExportMat> request) {

		LOGGER.info("MT_ExportMat id: {}", request.getValue().getBerichtHoofding().getID());
	}

}
