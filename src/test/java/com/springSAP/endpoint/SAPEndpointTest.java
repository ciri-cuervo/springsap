package com.springSAP.endpoint;

import static org.springframework.ws.test.server.RequestCreators.*;
import static org.springframework.ws.test.server.ResponseMatchers.*;

import java.nio.file.Files;
import java.nio.file.Paths;

import javax.xml.transform.Source;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.ws.test.server.MockWebServiceClient;
import org.springframework.xml.transform.StringSource;

import com.springSAP.config.SecurityConfig;
import com.springSAP.config.WebMvcConfig;
import com.springSAP.config.WebServiceConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { SecurityConfig.class, WebMvcConfig.class, WebServiceConfig.class })
@WebAppConfiguration
public class SAPEndpointTest {

	@Autowired
	private WebApplicationContext webApplicationContext;

	private MockWebServiceClient mockClient;

	@Before
	public void createClient() {
		mockClient = MockWebServiceClient.createClient(webApplicationContext);
	}

	@Test
	public void testValidTransaction() throws Exception {
		byte[] request = Files.readAllBytes(Paths.get("src/test/resources/webservice/MT_ExportMat_Valid_Request.xml"));
		Source requestSoapEnvelope = new StringSource(new String(request, "utf8"));

		mockClient.sendRequest(withSoapEnvelope(requestSoapEnvelope))
			.andExpect(noFault());
	}

	@Test
	public void testFaultTransaction() throws Exception {
		byte[] request = Files.readAllBytes(Paths.get("src/test/resources/webservice/MT_ExportMat_Bad_Request.xml"));
		Source requestSoapEnvelope = new StringSource(new String(request, "utf8"));

		mockClient.sendRequest(withSoapEnvelope(requestSoapEnvelope))
			.andExpect(clientOrSenderFault());
	}

}
